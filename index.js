console.log("Hello World!");

let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/

users[4] = "John Cena";
let data5 = users[4];


// function changeUsers(index){
// 	console.log(index);
// }

// changeUsers(users);

function addNewWrestler1(data1, data2, data3, data4){
	let addUsers1 = [data1, data2, data3, data4, data5];
	return addUsers1;
}
let newUsers1 = addNewWrestler1("Dwayne Johnson", "Steve Austin", "Kurt Angle", "Dave Bautista");
console.log(newUsers1);



/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/

// function changeUsers(index){
// 	console.log(index);
// }

// changeUsers(users[2]);

function chooseWrestler(index){
	let pickUsers2 = newUsers1[2];
	return pickUsers2;
}

let chosenUsers2 = chooseWrestler(newUsers1[2]);
console.log(chosenUsers2);






/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/
// console.log(users[users.length - 1]);

// users.length--;
// function changeUsers(index){
// 	console.log(index);
// }
// changeUsers(users);

function chooseWrestler2(index){
	let pickUsers3 = (newUsers1[newUsers1.length-1]);
	return pickUsers3;
}

let chosenUsers3 = chooseWrestler2(newUsers1[newUsers1.length-1]);
console.log(chosenUsers3);




newUsers1.length--;

function deleteWrestler(index){
	let pickUsers4 = (newUsers1);
	console.log(pickUsers4);
}

let newUsers4 = deleteWrestler(newUsers1);
console.log();

/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/

// users[3] = "Triple H";

// function changeUsers(index){
// 	console.log(index);
// }
// changeUsers(users);

newUsers1[3] = "Triple H";
function changeWrestler(index){
	let changeUser5 = (newUsers1);
	console.log(changeUser5);

}
let newUsers5 = changeWrestler(newUsers1);
console.log();

/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.

*/

// users = "[]"

// function changeUsers(index){
// 	console.log(index);
// }
// changeUsers(users);

newUsers1 = "[]";
function deleteAllWrestler(index){
	let changeUser6 = newUsers1;
	console.log(changeUser6);
}
let newUsers6 = deleteAllWrestler(newUsers1);
console.log();

/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/
for(let index=0; index<newUsers1.length; index++){

	if(newUsers1>0){
	console.log("false");
}

	else{
		console.log("true");
	}
}

let isUserEmpty = newUsers1;
